/*
 * Innstillinger: Variabler som kan/burde bestemmes av bruker. Verdiene her er anbefalte.
 */

bool serialLogging = false; //Aktiverer debug output på seriell port
bool fileLogging   = true;  //Aktiverer debug output på SD kort

//System settings:
int measFreq =  6;   //Målinger per min (6 er laveste anbefalte verdi fra Geofysisk Institutt)
int expFreq =  10;   //Antall minutter mellom eksportering (normal standard hos Geofysisk Institutt)
int savFreq =   5;   //Antall minutter mellom lagring av verdier

//Kommunikasjon:
String ssid;
String pass;
String serverHostName;
int keyIndex = 0;               //Nøkkelindeks for bruk med WEP.
//char serverHostName[] = "beta.bergensveret.no";   //Domenenavnet til server
int  serverPort = 8080; //Port til server (80 default)
//int  serverPort = 80; //Port til server (80 default)

String apiKey = "IzNnAnb1Zdv23Sn3";   //Nøkkel for identifisering i UiB sin server

char batteryType = 'V';   //%: Batteri status i prosent, V: Batterispenning

//Kalibrering:
float gaugeSize = 0.1;     //Regn per RainCnt i ml
float windConst = 2.2;     //Vind per windCnt.(Foreslått verdi)(1 WindCnt/s = WindConst m/s)
float tempComp = 0;        //Avvik temperatur ved kalibrering
float humComp = 0;         //Avvik fuktighet ved kalibrering
float presComp = 0;        //Avvik trykk ved kalibrering

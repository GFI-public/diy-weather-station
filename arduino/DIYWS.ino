/* 
 * DIY Værstasjon med lagring til SD-kort og ekportering til server.
 * Version: 1.2
 * 02.03.2019
 * 
 * Bacheloroppgave: BO17E-16, Do-it-yourself kit for an automated weather station
 * Høgskulen på Veslandet, 14HEEL
 * 
 * Oppdragsgiver: Universitetet i Bergen, Geofysisk avdeling
 * 
 * Forfattere:
 * Ruben Austefjord
 * Eirik Vivelid Stokke
 * Simen Norrheim Larsen
 * Harald Sodemann
*/

#include "Variables.h"                                                          //Inkluderer biblioteket "Variables.h"
#include "Settings.h"                                                           //Inkluderer biblioteket "Settings.h"

void serialPrint(String message)                                                  //Log bare hvis det er ønsket
{
  if (serialLogging==true) Serial.print(message);
  if (fileLogging==true)   {
    logfile = SD.open(logFileName, FILE_WRITE);
    if (logfile) {      
      logfile.print(message);
      logfile.close();
    }
  }
}

void serialPrintln(String message)
{
  if (serialLogging==true) Serial.println(message);
  if (fileLogging==true)   {
    logfile = SD.open(logFileName, FILE_WRITE);
    if (logfile) {      
      logfile.println(message);
      logfile.close();
    }
  }
}

void setup()                                                                    //Setup funksjonen: Kjøres en gang ved oppstart av systemet. Alle Arduino prosjekter må ha "void setup()" og "void loop()"
{
    Serial.begin(9600);                                                         //Starter Serial kommunikasjon, primært for å kunne bruke Serial Monitor(ctrl+shift+m)
    delay(2000);                                                                //Gir Serial.begin(9600) tid til å sette opp kommunikasjon

    pinMode(extLED, OUTPUT);                                                    //Definerer variabelen extLED som en utgangsport
    
    rtc.begin();                                                                //Starter kommunikasjon med RTC(Real Time Clock)

    Serial.println("Welcome to the UiB Do-It-Yourself Weather Station 1.2"); 
    Serial.println("-----------------------------------------------------");
    Serial.println("Starting system check.");                                   //"serialPrintln" skriver det som står inne i (" ... ") ut til Serial Monitor

    if (setupSD() != true)                                                      //Iverksett SD kort
    {
       Serial.println("  Could not access SD card reader. Restarting...");
       Serial.println();
       delay(2000);
       setup();
    }

    Serial.println("");
    if (read_config() == false)                                                 //Leser parameterne for wifi og stasjon fra fil "config"
    {
      Serial.println("Invalid configuration file. Please edit and restart.");
      Serial.println();
      delay(2000);
      setup();
    }
    serialLogging = true;

    Serial.println("");
    if (setupBME280() == false)                                                 //Iverksett p,T,RH sensor BME280
    {
       Serial.println("Could not initialize sensor BME280. Restarting...");
       Serial.println();
       delay(2000);
       setup();
    }
    Serial.println("");

    //System evaluation:
    if ((setupWiFi() && syncTime()) == true)                                    //Iverkssett setupWiFi() og syncTime()" funksjonene og sjekker om de har verdien "true"
    {                                                                           
      buUpdate();                                                               //Sørger for at gamle backuper på SD-kortet ikke blir sendt ved første gjennomføring
      Serial.println();
      Serial.println("System OK. Recording..");
      digitalWrite(extLED, LOW);                                                //Statusdiode lyser ikke dersom systemet startet opp som det skulle
    }
    else                                                                        //Kode som blir eksekvert dersom en eller flere av "(setupSD() && setupWiFi() && syncTime() && BMEstatus)" funksjonene returnerer "false"
    {
      digitalWrite(extLED, HIGH);                                               //Statusdioden lyser dersom systemet ikke startet som som det skulle
      Serial.println();
      Serial.println("System is compromized and will restart.");
      WiFi.disconnect();                                                        //Hvis setupWiFi skal kunne returnere "true" må den først kobles fra nettet
      delay(10000);                                                             //Gir tid for frakobling av WiFi
      setup();                                                                  //Starter setup() på nytt for å prøve og sette opp systemet på nytt
    }

    pinMode(A1, INPUT);                                                         //setter opp en fysisk kobling mellom en pin på kortet med en input variabel med verdien "A1"
    pinMode(rainPin, INPUT);                                                    //setter opp en fysisk kobling mellom en pin på kortet med en input variabel med verdien "rainPin"
    pinMode(windPin, INPUT);                                                    //setter opp en fysisk kobling mellom en pin på kortet med en input variabel med verdien "windPin"
    
    lastTHP = millis();                                                         //Resetter tellere slik at de funksjonaliteten er synkronisert.
}

void setInterrupt(bool state)                                                   //Enkelt aktivering/deaktivering av IRQ
{
   if (state==true) {
     attachInterrupt(digitalPinToInterrupt(rainPin), rainIRQ, FALLING);         //Setter opp interrupt funksjonalitet for den fysiske pinnen "rainPin"      
     attachInterrupt(digitalPinToInterrupt(windPin), windIRQ, FALLING);         //Setter opp interrupt funksjonalitet for den fysiske pinnen "windPin"
   } else {
     detachInterrupt(digitalPinToInterrupt(rainPin));
     detachInterrupt(digitalPinToInterrupt(windPin));
   }
}

void loop()                                                                     //Loop funksjonen: Starter etter fullført setup() og starter på ny når den er fullført 
{     
  //Measure:
  if(millisCheck(lastTHP) >= (60000/measFreq))                                  //Sjekker om tiden siden siste runde med målinger er lik bestemt tid for antall målinger per minutt
  {
    getTempHumPres();                                                           //Henter verdiene fra sensoren BME280 og lagrer disse i tabeller

    blinkLED();
    //printValues();                                                            //Skriver ut målinger til Serial Monitor
  }

 //Export:
  if (sdeCnt == expFreq)                                                        //Sjekker om antall minutter siden siste eksportering er likt bestemt antall minutter mellom hver eksportering
  {
    serialPrintln("");
    serialPrintln("Trying to export to web server");

    setInterrupt(false);                                                        //Mindre forstyrrelse av server-kommunikasjon uten interrupt
    expBackup();
    setInterrupt(true);

    statusLED();                                                                //Vurderer om LED skal indikere feil på system ut fra systemets statusvariabler    

    sdeCnt = 0;                                                                 // Nullstill denne for å vente til neste exporten
  }

  //Lagring:
  if (thpCnt/measFreq == savFreq)                                               //Sjekker om antall minutter siden siste eksportering er likt bestemt antall minutter mellom hver eksportering
  {
    serialPrintln("");
    serialPrintln("Trying to export to SD card");

    sdeCnt = sdeCnt+thpCnt/measFreq;                                          //Så at vi vet når data sendes til webserveren
    
    aveMeasurements();                                                          //Regner ut gjennomsnittlige verdier

    String expData = createPacket();                                            //Lager pakke av gjennomsnittlige målinger og tidsstempler den
    
    saveSD(weatherDataFileName, expData);                                       //Lagrer pakke på SD-kort
  
    backPackage(expData);                                                       //Eksporterer pakken til server og lagrer status på siste eksportering i expStatus
    //expPackage(expData);

    statusLED();                                                                //Vurderer om LED skal indikere feil på system ut fra systemets statusvariabler

    lastTHP = millis();                                                         //Oppdaterer tiden siden forrige eksportering ettersom tiden det tar å eksportere ikke skal regnes med i en syklus
  }

  delay(1000);     // vent et lite øyeblikk
}

//Funksjoner i setup:

bool setupWiFi()                                                                //Setter opp kommunikasjon med valgt nettverk i "settings.h"
{
    Serial.print("Attempting to connect to Wifi with SSID: ");
    Serial.println(ssid);
    
    for(int i = 0; i < 4; i++)                                                  //Prøver opptil 4 ganger på å koble til nettverket
    {
      status = WiFi.begin(ssid, pass);                                          //Prøver å sette opp kommunikasjon med nettverket "ssid" med passordet "pass"(settings.h)
      if(status != WL_CONNECTED)                                                //Så lenge denne ikke er tilkoblet gir vi den litt tid og skriver ut "." for å vise tegn til liv på Serial Monitor
      {
        delay(3000);
        serialPrint(".");
      }
      else
        break;                                                                  //Bryter ut av løkken dersom systmet får etablert tilkobling med nettverk
    }
    if(status != WL_CONNECTED)                                                  //Dersom systemet ikke klarer å koble til gitt WiFi returnerer funksjonen "false"
    {
      Serial.println("");
      Serial.println("    Unable to connect.");
      return false;
    }
    else
    {
      WiFi.maxLowPowerMode();
      printWiFiStatus();                                                        //Dersom tilkobling er vellykket skrives info om tilkoblingen ut på Serial Monitor
      return true;
    }
}

bool setupBME280()                                                              //Setter opp koblingen med sensoren BME280 og returnerer status
{
  Serial.println("Initializing sensor BME280...");                                 

  if (bme.begin())                                                              //Starter kommunikasjon med sensoren BME280 
  {
    Serial.println(" BME280 Initialized.");
    return true;                                                                //Returnerer true dersom "bme.begin()" går bra
  }
  else
  {
    Serial.println(" Could not find a valid BME280 sensor, check wiring!");
    return false;                                                               //Returnerer false dersom "bme.begin()" ikke går bra
  }
}

bool setupSD()                                                                  //Setter opp kommunikasjonen med SD-kort modul og returnerer status
{
    Serial.println("");
    Serial.println("Initializing SD card...");
    
    if (!SD.begin(chipSelect))                                                  //Setter opp tilkobling og sjekker om denne er vellykket
    {
      Serial.println("  Card failed or not present.");
      return false;
    }
    else
    {
      Serial.println("  SD card initialized.");
      return true;
    }
}

bool syncTime()                                                                 //Synkroniserer RTCen på kortet med klokken med en klokke på nettet 
{ 
  unsigned long epoch;                                                          //Variabel for lagring av hentet epoch
  int numberOfTries = 0, maxTries = 100;

  Serial.println("");
  Serial.print("Synchronising time.");
  
  do
  {
    epoch = WiFi.getTime();                                                     //Henter epoch fra NTP-server
    delay(1000);
    numberOfTries++;
    Serial.print(".");
  }
  while ((epoch == 0) && (numberOfTries <= maxTries));                          //Antall forsøk på å hente epoch
  Serial.println("");
  
  if(epoch == 0)                                                                //Funksjonen returnerer false dersom mottatt epoch ikke inneholder noen data
  {
    Serial.println("  NTP unreachable.");
    Serial.println("  Failed to synchronize time.");
    return false;
  }
  else
  {
    rtc.setEpoch(epoch);                                                        //Konverterer og opptaterer RTCen fra hentet epoch
    delay(255);
    
    if(rtc.getYear() == 00)                                                     //Enkel sjekk for om klokken har blitt synkronisert riktig
    {
       Serial.println("  Failed to synchronize time.");
       return false;
    }
    else
    {
      Serial.print("  Time synchronized: ");
      Serial.println(TimeStamp());
    }
    return true;
  }
}

//Underfunksjoner - Setup:

void printWiFiStatus()                                                          //Skriver ut info om nåværende nettverksstatus
{
  Serial.print("  Connected to Wifi with SSID: ");
  Serial.println(WiFi.SSID());                                                  //Navnet på tilkoblet nettverk
  
  IPAddress ip = WiFi.localIP();                                                //IP-adressen til tilkoblet nettverk
  Serial.print("  IP Address: ");
  Serial.println(ip);
  
  long rssi = WiFi.RSSI();                                                      //Signalstyrke
  Serial.print("  Signal strength (RSSI):");
  Serial.print(String(rssi));
  Serial.println(" dBm");
}



//Funksjoner i loop:

int millisCheck(unsigned long lastMillis)                                       //Returnerer forskjellen mellom nåværende verdi av millis() og "lastMillis"
{
  unsigned long Millis;
  Millis = millis();                                                            //millis() er en funksjon som returnerer andtall millisekunder siden starten av setup()
  
  if(Millis < lastMillis)                                                         
    return 4294967296 - lastMillis + Millis;                                    //Kompanserer for eventuell tilbakestilling av millis(). 4294967296 er maksimal verdi millis() kan returnere. Etter dette tilbakestilles millis() automatisk til 0
  else
    return Millis - lastMillis;
}

void getTempHumPres()                                                           //Lagrer målinger fra BME280
{
  serialPrint("+");
  thpCnt++;                                                                     //Øker antall målinger siden siste gjennomsnittsberegning med 1
  //if (bme.begin()) {
    Temp[thpCnt] = bme.readTemperature();
    Hum[thpCnt] = bme.readHumidity();
    Pres[thpCnt] = (bme.readPressure() / 100.0F);
  //}

  if (isnan(Temp[thpCnt])) {
    Temp[thpCnt]=-999.0;
  }
  if (isnan(Hum[thpCnt])) {
    Hum[thpCnt]=-999.0;
  }
  if (isnan(Pres[thpCnt])) {
    Pres[thpCnt]=-999.0;
  }

  lastTHP = millis();                                                           //Oppdaterer tiden for siste måling
}

void printValues()                                                              //Skriver ut sist målte verdier
{   
  serialPrint("T (C) = ");
  serialPrint(String(Temp[thpCnt]));
    
  serialPrint(", p (hPa) = ");
  serialPrint(String(Pres[thpCnt]));
    
  serialPrint(", RH (%) = ");
  serialPrint(String(Hum[thpCnt]));
     
  serialPrint(", sdeCnt = ");
  serialPrint(String(sdeCnt));

  serialPrint(", wind cnt: ");
  serialPrint(String(windCnt));
    
  serialPrint(", FF (m/s) = ");
  windAve = (windCnt * windConst)/((thpCnt/measFreq)*60);
  serialPrint(String(windAve));
    
  serialPrint(", rain cnt: ");
  serialPrint(String(rainCnt));
    
  serialPrint(", RR (mm/h) = ");
  serialPrint(String((rainCnt*gaugeSize)*(60/expFreq)));
    
  serialPrint(", B = ");
  serialPrintln(String(batteryStatus(batteryType ,batteryActivationPin)));           //Sjekker og skriver ut målte batteristatus
}

void aveMeasurements()                                                          //Regner ut gjennomsnittet av målingene lagret siden siste gjennomsnittsberegning
{ 
  serialPrintln("*aveMeasurements()");
  //Nullstiller data fra forrige gjennomsnittsberegning:
  tempAve = 0;
  humAve = 0;
  presAve = 0;
  windAve = 0;
  rainAve = 0;

  //Legger sammen lagrede målinger:
  for (int i = 1; i <= thpCnt; i++)
  {    
    tempAve = tempAve + Temp[i];
    humAve = humAve + Hum[i];
    presAve = presAve + Pres[i];
  }
  
  //Deler på antall målinger for å få gjennomsnitt:
  tempAve = (tempAve/thpCnt) + tempComp;
  humAve = (humAve/thpCnt) + humComp;
  presAve = (presAve/thpCnt) + presComp;
  
  if(windCnt != 0)
    windAve = (windCnt * windConst)/(expFreq * 60);
  if(rainCnt != 0)
    rainAve = (rainCnt*gaugeSize)*(60/expFreq);
    
  //Nullstiller tellere slik at de er klar for nye målinger:
  thpCnt = 0;
  windCnt = 0;
  rainCnt = 0;
}

String createPacket()                                                           //Lager pakke av gjennomsnittlige målinger og tidsstempler den
{
  serialPrintln("*createPacket()");
  return "{\"API-key\":\""+ apiKey +"\",\"data\":[{\"dt\":\""+ TimeStamp() +
          "\",\"P\":"+ presAve +",\"T\":"+ tempAve +",\"U\":"+ humAve +
          ",\"F\":"+ windAve +",\"R\":"+ rainAve +",\"B\":"
          + batteryStatus(batteryType ,batteryActivationPin) +",\"SD\":"
          + sdError +"}]}";
}

void saveSD(String fileName, String dataString)                                 //Lagrer en "pakke" med navnet "dataString" til filen "fileName" på SD-kortet
{ 
  serialPrintln("*saveSD()");
  serialPrintln("Saving to SD card:");
  serialPrint("  ");
  serialPrintln(dataString);
  serialPrint(" to file ");
  serialPrintln(fileName);

  if(digitalRead(chipDetect) == 0)                                              //Sjekker om SD-kortet er tilkoblet
  {
    serialPrintln("  SD card not found.");
    
    if(sdError == 0)                                                            //Dersom sdError ikke allerede er registrert skrives dette ut 
    {   
      serialPrintln("  Sending error mail.");
      sdError = 1;
      return;                                                                   //Bryter funksjonen slik at resten av koden i funksjonen ikke utføres
    }
    else                                                                        //Dersom sdError allerede har vært registrert skal rapporteres kun feil
    {
      serialPrintln("  Failed to save data.");
      return;
    }
  }
  else if((digitalRead(chipDetect) == 1) && (sdError == 1))                     //Dersom systemet finner kortet etter at det har vært registrert sdError må verdien settes tilbake til 0
  {
    serialPrintln("    Reconnected to SD card.");

    SD.begin(chipSelect);
    sdError = 0;
  }
    
  File dataFile = SD.open(fileName, FILE_WRITE);                                //Åpner filen på SD-kortet
  if(dataFile)
  {
    dataFile.println(dataString);                                               //Skriver "pakke" til åpnet fil
    dataFile.close();                                                           //Lukker filen
    serialPrintln("  Data saved.");
  }
  else
  {
    serialPrintln("  Failed to save data.");
    sdError = 1;
  }
}

void backPackage(String expData)                                                //Eksporterer pakkene til SD backup
{
  serialPrintln("*backPackage()");
  serialPrint("Exporting to SD backup file ");
  serialPrintln(backupFile);
        
  saveSD(backupFile, expData);                                                  //Lagrer pakke i backupfil dersom tilkobling til nettverk ikke er vellykket
  expStatus = false;
}

void expPackage(String expData)                                                 //Eksporterer pakkene til server hos UiB med håndering av feilende sendinger og lagring av backuper
{
  serialPrintln("*expPackage()");
  serialPrintln("");
  serialPrintln("Exporting to webserver..");
  
  // trying to reconnect to server
  if(WiFi.status() != WL_CONNECTED) {
    serialPrintln("    No network connection found. Trying to reconnect..");
     if (setupWiFi() == false) {
        serialPrint("No success. Backing up to SD backup file ");
        serialPrintln(backupFile);
        saveSD(backupFile, expData);                                            //Lagrer pakke i backupfil dersom tilkobling til nettverk ikke er vellykket
        expStatus = false;
        WiFi.end();
        return;
     }
  }
                                                       
  /*wifi.flush();                                                               //Lukker eventuelle åpne tilkoblinger
  wifi.stop(); 
  int maxRetry=5, tries=0;
  do {
    delay(3000);
    wifi.connect(serverHostName.c_str(), serverPort);
    tries++;
    serialPrint("-");
  } while(!wifi.connected() && (tries<maxRetry));                             //Venter til serveren(wifi) er klar til å sende en respons
  if (!wifi.connected()) {
    serialPrintln("Could not connect to wifi");
  }*/
  
  if (sendPackage(expData)==true) {                                            //Sender pakke og returnerer status
    serialPrintln("Export successfull.");
    expStatus = true;
    WiFi.end();
    return;
  } else {
    serialPrintln("Export failed.");
    serialPrint("No success. Backing up to SD backup file ");
    serialPrintln(backupFile);
    saveSD(backupFile, expData);                                            //Lagrer pakke i backupfil dersom tilkobling til nettverk ikke er vellykket
    expStatus = false;
    WiFi.end();
    return;
  }
}

void expBackup()                                                                //Eksporterer backupene lagret på siste backupfil
{
  serialPrintln("*expBackup()");
  if (SD.exists(backupFile) == true)                                            //Dersom det finnes en backupfil skal denne sendes
  {
    serialPrintln("");
    File BUFile = SD.open(backupFile,FILE_READ);                                //Henter backupfil fra SD-kort
     
    if (BUFile)
    {     
      serialPrint("Backed up data found in file: ");
      serialPrintln(backupFile);

      // trying to reconnect to server
      if(WiFi.status() != WL_CONNECTED) {
        serialPrintln("    No network connection found. Trying to reconnect..");
          if (setupWiFi() == false) {
            serialPrint("No success. Continuing to measure.");    
            BUFile.close();       
            WiFi.end();
            return;
        }
     }

     /*int maxRetry=5, tries=0; 
     wifi.flush();                                                           //Lukker eventuelle åpne tilkoblinger
     wifi.stop(); 
     do {
       delay(3000);
       wifi.connect(serverHostName.c_str(), serverPort);
       tries++;
       serialPrint("-");
     } while(!wifi.connected() && (tries<maxRetry));                         //Venter til serveren(wifi) er klar til å sende en respons

     if (!wifi.connected()) {
       serialPrintln("Could not connect wifi to server");
     }*/

     expStatus = true;
     unsigned int sendLines = 0;
     while (BUFile.available())                                                //Eksporterer data fra backupdata pakke for pakke
     {
       String RetrievedData = BUFile.readStringUntil('\n');                    //Leser av en linje/pakke

       if(sendPackage(RetrievedData) == false)  {                              //Eksporterer hentet pakke
         expStatus = false;
         serialPrintln("Could not export "+RetrievedData);
         serialPrintln("Continuing to measure.");
         /*wifi.flush();
         wifi.stop();*/
         WiFi.end();
         if (sendLines==0) {
          BUFile.close();
          return;
         }

         serialPrintln("Removing "+String(sendLines)+" lines that was send from backup file");
         String oldBackupFile = backupFile;
         buUpdate();
         File BUBackFile = SD.open(backupFile,FILE_WRITE);                     // Flytter resterende data til ny bakupfil
         if (BUBackFile == true) {
           BUBackFile.println(RetrievedData);
           while (BUFile.available())                                          //Eksporterer data fra backupdata pakke for pakke
           {
             String RetrievedData = BUFile.readStringUntil('\n');
             BUBackFile.println(RetrievedData);
           }
           BUBackFile.close();
           SD.remove(backupFile);                                              //move remaining file
         }
         BUFile.close();                                                       //Lukker backupfil
         return;
       }
       delay(1000);
       sendLines++;
     }
     BUFile.close();                                                           //Lukker backupfil
     /*wifi.flush();
     wifi.stop();*/
                                                                               //Lukker eventuelle åpne tilkoblinger
     if (expStatus == true)
     {
       serialPrintln("Backup exported."); 
       SD.remove(backupFile);
       buCurrent();
       if (buNumb>0) {
         expBackup();                                                          //Send enda gamlere backupfil også
       }
       buUpdate();
     } else {
       serialPrintln("Export of backup failed.");
     }
   }
   WiFi.end();
  }
}

void blinkLED()
{
  digitalWrite(extLED, HIGH);
  delay(250);
  digitalWrite(extLED, LOW);
}

void statusLED()                                                               //Vurderer om estern LED skal indikere feil på system ut fra systemets statusvariabler
{
  serialPrintln("*statusLED()");
  if((expStatus == false) || (sdError == 1))                                   //Dersom enten eksportering eller lagring til SD-kort ikke fungerer som det skal blinker eksterne dioden 3 ganger
  {
    digitalWrite(extLED, HIGH);
    delay(250);
    digitalWrite(extLED, LOW);
    delay(250);
    digitalWrite(extLED, HIGH);
    delay(250);
    digitalWrite(extLED, LOW);
  } else {                                                                     //Dersom systemet lagrer og sender som det skal blinker dioden en gang
    digitalWrite(extLED, HIGH);
    delay(250);
    digitalWrite(extLED, LOW);
  }
}

//Underfunksjoner - loop:

String TimeStamp()                                                             //Henter ut data fra den intergrerte RTCen på kortet og setter denne på rett format
{
  serialPrintln("*TimeStamp()");
  String TempSec = ":"; TempSec = TempSec + print2digits(rtc.getSeconds());
  String TempMin = ":"; TempMin = TempMin + print2digits(rtc.getMinutes());
  String TempHour = " "; TempHour = TempHour + print2digits(rtc.getHours());
  String TempDay = "-"; TempDay = TempDay + print2digits(rtc.getDay());
  String TempMonth = "-"; TempMonth = TempMonth + print2digits(rtc.getMonth());
  String TempYear = "20"; TempYear = TempYear + print2digits(rtc.getYear());

  return TempYear + TempMonth + TempDay + TempHour + TempMin + TempSec;
}

bool sendPackage(String package)                                               //Setter opp tilkobling mot server og sender pakken "package"
{
  serialPrintln("*sendPackage()");
  String response = "";

  /*int maxRetry=3, tries=0; 
  while (!wifi.connected() && (tries<maxRetry)) {
    wifi=WiFi;
    tries++;
    delay(1000);
    serialPrint("-");
  }                                                    //Venter til serveren(wifi) er klar til å sende en respons
  if (!wifi.connected()) {
     serialPrintln("Could not connect wifi to server");
     return false;
  }*/

  HttpClient client = HttpClient(wifi, serverHostName.c_str(), serverPort);
  
  serialPrintln("Sending following package: ");
  serialPrint("  ");
  serialPrintln(package);

  delay(1000);                                                                   //Ved sending av mye data på kort tid kan det av forskjellige årsaker være lurt å vente litt mellom hver pakke
  
  //if(wifi.connected())                                                        //Kobler til serveren "serverHostName" på port 80(standard port)
  //{
    serialPrintln("  Sending..");

    //Skriver til wifien på standard HTTP-form:
    /*wifi.println("POST /api/diy/obs HTTP/1.0");
    wifi.print("Host: ");
    wifi.println(serverHostName);
    wifi.println("Content-Type: application/json");
    wifi.print("Content-Length: ");
    wifi.println(package.length());
    wifi.println();
    wifi.println(package);
    wifi.println();
    wifi.println(F("Connection: close\r\n"));*/
    String contentType = "application/json";
    String postData = package;

    client.post("/api/diy/obs", contentType, postData);

    // read the status code and body of the response
    int statusCode = client.responseStatusCode();
    //String response = client.responseBody();

    Serial.print("Status code: ");
    Serial.println(statusCode);
    Serial.print("Response: ");
    Serial.println(response);

    Serial.println("Wait five seconds");
    delay(5000);
  /*}
  else
  {
    serialPrintln("  Unable to connect to server.");
    return false;
  }*/

  /*delay(2000);
  while (wifi.available())                                                    //Leser repons fra serveren så lenge denne er tilkjengelig
  {
    char c = wifi.read();                                                     //Leser karakter fra serveren
    response.concat(c);                                                         //Lagrer lest karakter til stringen "response"
  }
  wifi.flush();
  .stop();
  serialPrint("  Received response '");
  serialPrint(response);
  serialPrintln("'");*/
  
  //if(response.startsWith("HTTP/1.1 204") || response.startsWith("HTTP/1.0 204"))                                       //Sjekker responsen fra serveren etter ønsket tilbakemelding
  if (statusCode==204)
  {
    serialPrintln("  Package delivered.");
    return true;
  }
  else if (statusCode==400)                                 //Dersom responsen inneholder koden 400 var ikke pakken som ble forsøkt levert på rett format/størrelse
  {
    serialPrintln("  Faulty package not delivered.");                           //Dersom pakken var på feil format eller størrelse er det ingen vits i å lagre denne til backup da denne ikke vil kunne sendes uansett
    return true;                                                                //Returnerer "true" selv om pakken ikke ble levert slik at filen slettes og den feilede pakken med den
  }
  else
  {
    serialPrint("  Unable to send package.");
    return false;
  }
}

void rainIRQ()                                                                  //Interruptfunksjon for registrering av pulser fra regnsensor
{
  rMillis = millis();
  if((rMillis - lastRain) > 200)                                               //Enkel funksjon for fjerning av prell(prell kan gi flere målinger per puls)
  {
    lastRain =rMillis;
    rainCnt++;
  }
}

void windIRQ()                                                                  //Interruptfunksjon for registrering av pulser fra vindsensor
{
  wMillis = millis();
  if((wMillis - lastWind) > 6.25)                                                //Enkel funksjon for fjerning av prell(prell kan gi flere målinger per puls)
  {
    lastWind = wMillis;
    windCnt++;
  }
}

String print2digits(int number)                                                 //Sørger for at datoene fra RTC kommer med riktig antall siffer
{
  String digit = "0";
  if (number < 10)                                                              //Dersom verdien fra RTC kun inneholder et siffer legges det til en 0 foran sifferet
    return String(digit + number);
  else
    return String(number);
}

float batteryStatus(char type, int actPin)                                      //Håndeterer porter for måling av betterispinngen, konverterer målinger og returnerer enden som målt spenning eller prosent
{
  serialPrintln("*batteryStatus()");
  int analogValue;
  float vComp = (3.19*1.28)/1023;                                               //Konstant for konvertering av lest analog verdi til tilsvarende spenning. Tar hensyn til spenningsdelingen.
  float voltage;
  static float vMax = 3.6, vMin = 3.1, vSpan;
 
  pinMode(actPin, OUTPUT);                                                      //Setter actPin til som utgang
  digitalWrite(actPin, LOW);                                                    //Drar actPin til jord for å kunne måle spenninsforsjellen mot jord
  analogValue = analogRead(A1);                                                 //Leser av verdien i spenningsdelingen
  pinMode(actPin, INPUT);                                                       //Setter actPin som en input. Dette gjør at den får en veldig stor impedans og dermed trekkes det ikke nevneverdig strøm når man ikke måler batterispeningen

  voltage  = analogValue * vComp;                                               //Konverterer lest verdi til virkelig spenning
  
  if ((voltage > 2.9) && (voltage < 4.2))                                       //Sjekker om målt spenning har fornuftig verdi
  {
    if (voltage > vMax)                                                         //Oppdaterer maksimal spenning dersom denne er høyere enn tidlegere målt spenning
      vMax = voltage;
    else if (voltage < vMin)                                                    //Oppdaterer minimal spenning dersom denne er høyere enn tidlegere målt spenning
      vMin = voltage;
  }
  else
    voltage = 10;                                                               //Dersom målt spenning ikke har fornuftig verdi settes denne til en urealistisk verdi som server kan motta

  vSpan = vMax - vMin;                                                          //For beregning av batteriprosent må man vite spenningsområdet til batteriet
   
  if(type == 'V')                                                               //Retunerer målt spenning dersom "type" = V
    return voltage;
  else if(type == '%')                                                          //Returnerer batteristatus som prosent dersom "type" = %
    return (100 * voltage)/vSpan;
}

void buUpdate()                                                                   //Synkroniserer backup nummer med antall backuper på SD-kortet slik at man ikke sender backuper som allerede er sendt
{
  serialPrintln("*buUpdate()");
  serialPrintln("");
  buNumb=0;
  do {
    buNumb++;
    backupFile = "BU" + String(buNumb) + ".txt";
  } while(SD.exists(backupFile));
  serialPrint("Next backupfile: ");
  serialPrintln(backupFile);
}

void buCurrent()                                                                   //Synkroniserer backup nummer med antall backuper på SD-kortet slik at man ikke sender backuper som allerede er sendt
{
  serialPrintln("*buCurrent()");
  serialPrintln("");
  buNumb=0;
  do {
    buNumb++;
    backupFile = "BU"+String(buNumb) + ".txt";
  } while(SD.exists(backupFile));
  buNumb--;
  backupFile = "BU"+String(buNumb)+".txt";
  serialPrint("Found backupfile: ");
  serialPrintln(backupFile);
}

bool read_config()
{
  String sval,skey;
  
  Serial.println("Reading configuration from SD card reader...");              
    
  if (SD.exists(configFile) == true)                                            //Dersom det finnes en backupfil skal denne sendes
  {
    File CFile = SD.open(configFile,FILE_READ);                                //Henter backupfil fra SD-kort
     
    if (CFile)
    {     
      Serial.print("  Settings found in file: ");
      Serial.println(configFile);
      int eqPos=0;

      while (CFile.available())                                          //Eksporterer data fra backupdata pakke for pakke
      {
        String line = CFile.readStringUntil('\n');

        eqPos=line.indexOf('=');
        
        if(line.charAt(0) == '#') 
        {
          // ignore comments
          //Serial.print("Comment found reading");
          //Serial.println(line);
        } else
          if(eqPos<0)
        {
          // error, no equal sign
          Serial.println("Warning: Config file entry without comment and equal sign.");
          Serial.println(line);
        } else {
          // extract the key word
          skey = line.substring(0,eqPos-1);
          skey.trim();
          sval = line.substring(eqPos+1);
          sval.trim();
          /*Serial.print(skey);
          Serial.print(" --- ");
          Serial.println(sval);*/

          // parse all known keys
          if (skey=="ssid") {
            ssid=sval;
          } else if (skey == "pass") {
            pass=sval;
          } else if (skey == "serverHostName") {
            serverHostName=sval;            
          } else if (skey == "keyIndex") {
            keyIndex=sval.toInt();
          } else if (skey == "serverPort") {
            serverPort=sval.toInt();
          } else if (skey == "apiKey") {
            apiKey=sval;
          } else if (skey == "measFreq") {
            measFreq=sval.toInt();
          } else if (skey == "expFreq") {
            expFreq=sval.toInt();
          } else if (skey == "savFreq") {
            savFreq=sval.toInt();
          } else if (skey == "fileLogging") {
            fileLogging=(sval.toInt()==1); // creates boolean value
          } else if (skey == "gaugeSize") {
            gaugeSize=sval.toFloat();
          } else if (skey == "windConst") {
            windConst=sval.toFloat();
          } else if (skey == "tempComp") {
            tempComp=sval.toFloat();
          } else if (skey == "humComp") {
            humComp=sval.toFloat();
          } else if (skey == "presComp") {
            presComp=sval.toFloat();
          } else {
            if (skey!="" && sval!="") {
              Serial.print("Unknown key/value pair: ");
              Serial.print(skey);
              Serial.print(" = ");
              Serial.println(sval);
            }
          }
        }
      }
    } else {
      Serial.println("No valid settings file found on SD card. Please create one according to installation instructions");
      return false;
    }
  } else {
    Serial.println("No valid settings file found on SD card. Please create one according to installation instructions");
    return false;
  }

  return true;
}


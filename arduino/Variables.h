/*
 * Systemvariabler: Variabler som er bestemt. Ved følging av manual skal det ikke være behov for å endre disse
 */

//Biblioteker: Standard Arduino
#include <SD.h>                 //Bibliotek for tilkobling med SD-kort modul
#include <Wire.h>               //Bibliotek for bla. anent håndtering av inn/utsignal over pins
#include <SPI.h>                //Bibliotek for kommunikasjon over SPI

//Biblioteker: Nedlastede
#include <Adafruit_Sensor.h>    //Bibliotek for bruk av Adafruit sine sensorer
#include <Adafruit_BME280.h>    //Bibliotek for kommunikasjon med BME280 SPI
#include <ArduinoHttpClient.h>  //Bibliotek for kommunikasjon med database via HTTP
#include <WiFi101.h>            //Bibliotek for oppsett av WiFi og kommunikasjon med eksterne servere
#include <WiFiUdp.h>            //Bibliotek for kommunikasjon over WiFi
#include <RTCZero.h>            //Bibliotek for bruk av den integrerte RTC'en(Real Time Clock) på kortet

RTCZero rtc;
WiFiClient wifi;

//BME280:
/*
#define BME_SCK 12
#define BME_MISO 13
#define BME_MOSI 11
#define BME_CS 10
*/                      //Dersom en ønsker å bruke BME280 over SPI kommunikasjon

#define SEALEVELPRESSURE_HPA (1036)   //Hvis man skulle ønske det er det mulig å beregne moh.

//Valg av kommunikasjonsmåte med BME280:
Adafruit_BME280 bme;                                        //I2C
//Adafruit_BME280 bme(BME_CS);                              //Hardware SPI
//Adafruit_BME280 bme(BME_CS, BME_MOSI, BME_MISO, BME_SCK); //Software SPI

//Nettverk:
int status = WL_IDLE_STATUS;                      //Statusindikator for sjekking av tilkobling

//SD-kort
File logfile;                      //Logfile for feilmeldinger etc.
String logFileName = "LOG.txt";    //Filnavn

//Værdata:
float Temp[1024];     //Lagrede målinger siden siste gjennnomsnittsberegning
float Hum[1024];      //Lagrede målinger siden siste gjennnomsnittsberegning
float Pres[1024];     //Lagrede målinger siden siste gjennnomsnittsberegning
float minWind[512];   //Lagrede målinger siden siste gjennnomsnittsberegning

float tempAve = 0, humAve = 0, presAve = 0, windAve = 0, rainAve = 0;   //Utregnede gjennomsnitt

int thpCnt = 0;       //Antall målinger av Temp/Fukt/Trykk siden siste gjennomsnittsmåling
int sdeCnt = 0;       //Antall målinger lagred siden siste export til webserveren
int rainCnt = 0;      //Antall pulser fra regnsensor siden siste gjennomsnittsberegning 
int windCnt = 0;      //Antall pulser fra vindsensor siden siste gjennomsnittsberegning

//Pins:
const int chipSelect = 4;   //Pin for tilkobling av chipSelect på SD-kort modul
const int chipDetect = 2;   //Pin for tilkobling av chipDetect på SD-kort modul
const int rainPin = 5;      //Pin for tilkobling av regnsensor
const int windPin = 7;      //Pin for tilkobling av vindsensor
const int extLED = 3;       //Pin for tilkobling av ekstern LED
const int batteryActivationPin = 14;   //Pinnummer for jording ved måling av betterispenning

//Generelle systemvariabler:
bool expStatus = true;    //Statusindikator for vellykket eksportering av data
int sdError = 0;          //Statusindikator for problemer med SD-kort(må være int da server skal motta verdi 0/1

unsigned long lastTHP;    //Verdi av millis() lagret ved siste måling av temp/fukt/trykk

// For IRQ routiner
static unsigned long lastRain = 0;
static unsigned long lastWind = 0;
unsigned long rMillis = 0;
unsigned long wMillis = 0;

const String weatherDataFileName = "wdata.txt";   //Navn på fil med værdata(kun små bokstaver og maksimalt 8 tegn)

String backupFile = "backup.txt";   //Navn på backupfil
int buNumb = 0;                     //Antall backupfiler på SD-kort

String configFile = "config.txt";   //Navn på konfigurasjonsfil



from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import SocketServer
import subprocess
import json
import urlparse


class RequestHandler(BaseHTTPRequestHandler):

  def _set_headers(self):
    self.send_response(204)
    print "---answer send---------------"
    self.send_header('Content-type', 'text/html')
    self.end_headers()

  def do_GET(self):
    self.send_response(204)
    self.end_headers()
    self.wfile.write('Pong!')

  def do_PUT(self):
    print "----- SOMETHING WAS PUT!! ------"
    length = int(self.headers['Content-Length'])
    content = self.rfile.read(length)
    self.send_response(204)
    print content

  def do_POST(self):
    print "---POST received---------------"
    self._set_headers()
    self.send_response(204)
    self.end_headers()
    self.wfile.write('<html><body>data well received</body></html>')
    print self.headers
    self.data_string = self.rfile.read(int(self.headers['Content-Length']))

    data = json.loads(self.data_string)
    with open("webserver_data.json", "a") as outfile:
        json.dump(data, outfile)
        #print("\n")
    print "{}".format(data)
    #f = open("for_presen.py")
    #self.wfile.write(f.read())
    return RequestHandler

def run(server_class=HTTPServer, handler_class=RequestHandler, port=8080):
  server_address = ('', port)
  httpd = server_class(server_address, handler_class)
  print 'Starting httpd...'
  httpd.serve_forever()

if __name__ == "__main__":
    from sys import argv

    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()
   
# fin
